# diagnostics

## Usage
```
helm install --name diag .
```

Mount a secret for debugging:
```
helm install --name diag . \
  --set secrets[0].name=consul-certs \
  --set secrets[0].type=secret \
  --set secrets[0].secretName=consul-certs 
```
