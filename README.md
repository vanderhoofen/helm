# my Helm charts

Various Helm charts for working with Kubernetes

## How do I install this repo?
```bash
helm repo add vanderhoofen https://vanderhoofen.gitlab.io/helm
```

## How do I install a helm chart?
```bash
helm install --name diag vanderhoofen/diagnostics
```

